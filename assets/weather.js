(function(){
	var app = angular.module("weatherReport",[]);
	
	var MainController = function($scope,$http){
		var getWeatherCallBack = function(response){
			console.log(response.data);
			$scope.weatherRprt = response.data;  
		};
		
		var getWeather = function(url){
			$http.get(url).then(getWeatherCallBack,onError);  
		};
		
		var getcoordinates = function(position) {
			var lat = position.coords.latitude;;
			var long = position.coords.longitude;
			var units=localStorage.getItem("Units");
			var CurrentWeatherURL = "http://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+long+"&units="+units+"&appid=2de143494c0b295cca9337e1e96b00e0";
			if (units == "imperial") {
				getWeather(CurrentWeatherURL);
			}
			else {
				getWeather(CurrentWeatherURL);
			}
		};
		
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(getcoordinates);
		}
		else {
			console.log("Geolocation is not supported by this browser.");
		}
		

		var onError = function(reason){
			$scope.error = "Could not fetch the user";
		};
		
		$scope.weatherRprt = {"weather":[{"icon": "01d"}]};
	};
	
	app.controller("MainController",["$scope","$http",MainController]);
}());

function SetCelsius()
{
	try
	{
		localStorage.Units = "metric";
		location.reload();
	}
	catch(e)
	{
		console.log(e.message);
	}
}
function SetFahrenheit() 
{
	try
	{
		localStorage.Units = "imperial";
		location.reload();
	}
	catch(e)
	{
		console.log(e.message);
	}
}
